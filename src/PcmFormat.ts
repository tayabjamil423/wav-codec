export class PcmFormat {
  readonly bytesPerSample: number;
  readonly frameSize: number;
  readonly byteRate: number;

  constructor(
    readonly sampleRate: number = 8000,
    readonly bitDepth: number = 16,
    readonly numChannels: number = 1
  ) {
    this.bytesPerSample = bitDepth / 8;
    this.frameSize = this.bytesPerSample * numChannels;
    this.byteRate = sampleRate * this.frameSize;
  }

  samplesPerChannel(dataLength: number): number {
    return dataLength / this.frameSize;
  }

  byteLengthFromDurationMs(durationMs: number): number {
    const durationS = durationMs / 1000;
    return durationS * this.byteRate;
  }

  durationMsFromByteLength(byteLength: number): number {
    const durationS = byteLength / this.byteRate;
    return durationS * 1000;
  }

  byteLengthFromSamples(samples: number): number {
    return this.frameSize * samples;
  }

  durationMsFromSamples(samples: number): number {
    const durationS = samples / this.sampleRate;
    return durationS * 1000;
  }

  samplesFromDurationMs(durationMs: number): number {
    const durationS = durationMs / 1000;
    return durationS * this.sampleRate;
  }
}
