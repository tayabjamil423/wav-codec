import { PcmFormat } from "./PcmFormat";

export function generateWavHeader(
  format: PcmFormat,
  dataLength: number
): Buffer {
  const header = Buffer.alloc(44);
  header.write("RIFF", 0); // RIFF Header
  header.writeUInt32LE(dataLength + 36, 4); // Chunk Size
  header.write("WAVE", 8); // Format
  header.write("fmt ", 12); // Subchunk 1 ID
  header.writeUInt32LE(16, 16); // Subchunk 1 Size
  header.writeUInt16LE(1, 20); // Audio format (1 is PCM)
  header.writeUInt16LE(format.numChannels, 22); // Number of channels
  header.writeUInt32LE(format.sampleRate, 24); // Sample rate
  header.writeUInt32LE(format.byteRate, 28); // Byte rate
  header.writeUInt16LE(format.frameSize, 32); // Block align
  header.writeUInt16LE(format.bitDepth, 34); // Bits per sample
  header.write("data", 36); // Subchunk 2 ID
  header.writeUInt32LE(dataLength, 40); // Subchunk 2 size
  return header;
}

export function encodePcmData(format: PcmFormat, pcmData: Buffer): Buffer {
  const header: Buffer = generateWavHeader(format, pcmData.length);
  return Buffer.concat([header, pcmData]);
}
