import { PcmFormat } from "./PcmFormat";

export function decodePcmData(format: PcmFormat, data: Buffer): Float32Array[] {
  const bytesPerSample: number = format.bytesPerSample;
  const samplesPerChannel: number = format.samplesPerChannel(data.length);

  const decoded: Float32Array[] = new Array(format.numChannels).fill(
    new Float32Array(samplesPerChannel)
  );

  let samplePos = 0;
  for (let sampleIndex = 0; sampleIndex < samplesPerChannel; sampleIndex++) {
    for (
      let channelIndex = 0;
      channelIndex < format.numChannels;
      channelIndex++
    ) {
      let decodedSample: number;
      switch (format.bitDepth) {
        case 8:
          decodedSample = data.readUint8(samplePos) - 128;
          decodedSample =
            decodedSample < 0 ? decodedSample / 128 : decodedSample / 127;
          break;
        case 16:
          decodedSample = data.readInt16LE(samplePos);
          decodedSample =
            decodedSample < 0 ? decodedSample / 32768 : decodedSample / 32767;
          break;
        default:
          throw new Error("Bit depth is not supported.");
      }
      decoded[channelIndex][sampleIndex] = decodedSample;
      samplePos += bytesPerSample;
    }
  }

  return decoded;
}
