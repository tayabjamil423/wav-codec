# wav-codec

Provides the `PcmFormat` construct and functions for encoding and decoding PCM audio data.