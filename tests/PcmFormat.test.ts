import { PcmFormat } from "../src/PcmFormat";

describe("PcmFormat", () => {
  let pcmFormat: PcmFormat;

  beforeEach(() => {
    pcmFormat = new PcmFormat(); // Using default values
  });

  test("should initialize with default values", () => {
    expect(pcmFormat.sampleRate).toBe(8000);
    expect(pcmFormat.bitDepth).toBe(16);
    expect(pcmFormat.numChannels).toBe(1);
    expect(pcmFormat.bytesPerSample).toBe(2); // 16 bits / 8 = 2 bytes
    expect(pcmFormat.frameSize).toBe(2); // 2 bytes/sample * 1 channel
    expect(pcmFormat.byteRate).toBe(16000); // 8000 samples/sec * 2 bytes/sample
  });

  test("should calculate samples per channel correctly", () => {
    const dataLength = 16000; // bytes
    const result = pcmFormat.samplesPerChannel(dataLength);
    expect(result).toBe(8000); // 16000 bytes / 2 bytes per frame = 8000 samples
  });

  test("should calculate byte length from duration in ms correctly", () => {
    const durationMs = 1000; // 1 second
    const result = pcmFormat.byteLengthFromDurationMs(durationMs);
    expect(result).toBe(16000); // 1 second * 16000 bytes/sec = 16000 bytes
  });

  test("should calculate byte length from samples correctly", () => {
    const samples = 8000; // number of samples
    const result = pcmFormat.byteLengthFromSamples(samples);
    expect(result).toBe(16000); // 8000 samples * 2 bytes per frame = 16000 bytes
  });

  test("should calculate duration in ms from byte length correctly", () => {
    const byteLength = 16000; // bytes
    const result = pcmFormat.durationMsFromByteLength(byteLength);
    expect(result).toBe(1000); // 16000 bytes / 16000 bytes/sec = 1 second = 1000 ms
  });

  test("should calculate duration in ms from samples correctly", () => {
    const samples = 8000; // number of samples
    const result = pcmFormat.durationMsFromSamples(samples);
    expect(result).toBe(1000); // 8000 samples / 8000 samples/sec = 1 second = 1000 ms
  });

  test("should calculate samples from duration in ms correctly", () => {
    const durationMs = 1000; // 1 second
    const result = pcmFormat.samplesFromDurationMs(durationMs);
    expect(result).toBe(8000); // 1 second * 8000 samples/sec = 8000 samples
  });
});
