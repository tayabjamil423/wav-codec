import { PcmFormat } from "../src/PcmFormat";
import { generateWavHeader, encodePcmData } from "../src/PcmEncode";

describe("WAV Encoding", () => {
  const defaultFormat = new PcmFormat(); // Default values: sampleRate=8000, bitDepth=16, numChannels=1

  it("should generate correct WAV header with default format", () => {
    const dataLength = 8; // 4 samples, 2 bytes per sample, 1 channel
    const header = generateWavHeader(defaultFormat, dataLength);

    // Verify header values
    expect(header.toString("utf8", 0, 4)).toBe("RIFF"); // RIFF Header
    expect(header.readUInt32LE(4)).toBe(dataLength + 36); // Chunk Size
    expect(header.toString("utf8", 8, 12)).toBe("WAVE"); // Format
    expect(header.toString("utf8", 12, 16)).toBe("fmt "); // Subchunk 1 ID
    expect(header.readUInt32LE(16)).toBe(16); // Subchunk 1 Size
    expect(header.readUInt16LE(20)).toBe(1); // Audio format (1 is PCM)
    expect(header.readUInt16LE(22)).toBe(defaultFormat.numChannels); // Number of channels
    expect(header.readUInt32LE(24)).toBe(defaultFormat.sampleRate); // Sample rate
    expect(header.readUInt32LE(28)).toBe(defaultFormat.byteRate); // Byte rate
    expect(header.readUInt16LE(32)).toBe(defaultFormat.frameSize); // Block align
    expect(header.readUInt16LE(34)).toBe(defaultFormat.bitDepth); // Bits per sample
    expect(header.toString("utf8", 36, 40)).toBe("data"); // Subchunk 2 ID
    expect(header.readUInt32LE(40)).toBe(dataLength); // Subchunk 2 size
  });

  it("should encode PCM data to WAV format with default format", () => {
    const pcmData = Buffer.from([
      0,
      0, // Sample 1: 0
      0,
      128, // Sample 2: -1
      255,
      127, // Sample 3: 1
    ]);
    const wavData = encodePcmData(defaultFormat, pcmData);

    // Check header
    const header = wavData.subarray(0, 44);
    expect(header.toString("utf8", 0, 4)).toBe("RIFF");
    expect(header.readUInt32LE(4)).toBe(pcmData.length + 36); // Chunk Size
    expect(header.toString("utf8", 8, 12)).toBe("WAVE");
    expect(header.toString("utf8", 12, 16)).toBe("fmt ");
    expect(header.readUInt32LE(16)).toBe(16);
    expect(header.readUInt16LE(20)).toBe(1);
    expect(header.readUInt16LE(22)).toBe(defaultFormat.numChannels);
    expect(header.readUInt32LE(24)).toBe(defaultFormat.sampleRate);
    expect(header.readUInt32LE(28)).toBe(defaultFormat.byteRate);
    expect(header.readUInt16LE(32)).toBe(defaultFormat.frameSize);
    expect(header.readUInt16LE(34)).toBe(defaultFormat.bitDepth);
    expect(header.toString("utf8", 36, 40)).toBe("data");
    expect(header.readUInt32LE(40)).toBe(pcmData.length);

    // Check PCM data
    const encodedPcmData = wavData.subarray(44);
    expect(encodedPcmData).toEqual(pcmData);
  });
});
