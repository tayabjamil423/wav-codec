import { generateWavHeader, encodePcmData } from "../src/PcmEncode"; // Replace with your actual file path
import { PcmFormat } from "../src/PcmFormat";

describe("WAV Header and PCM Data Encoding", () => {
  let pcmFormat: PcmFormat;

  beforeEach(() => {
    pcmFormat = new PcmFormat(); // Using default values
  });

  const validateWavHeader = (header: Buffer, dataLength: number) => {
    expect(header.subarray(0, 4).toString()).toBe("RIFF");
    expect(header.readUInt32LE(4)).toBe(dataLength + 36); // Chunk Size
    expect(header.subarray(8, 12).toString()).toBe("WAVE");
    expect(header.subarray(12, 16).toString()).toBe("fmt ");
    expect(header.readUInt32LE(16)).toBe(16); // Subchunk 1 Size
    expect(header.readUInt16LE(20)).toBe(1); // Audio format (PCM)
    expect(header.readUInt16LE(22)).toBe(1); // Number of channels
    expect(header.readUInt32LE(24)).toBe(8000); // Sample rate
    expect(header.readUInt32LE(28)).toBe(16000); // Byte rate
    expect(header.readUInt16LE(32)).toBe(2); // Block align
    expect(header.readUInt16LE(34)).toBe(16); // Bits per sample
    expect(header.subarray(36, 40).toString()).toBe("data");
    expect(header.readUInt32LE(40)).toBe(dataLength); // Subchunk 2 size
  };

  describe("generateWavHeader", () => {
    test("should generate a correct WAV header with default PcmFormat values", () => {
      const dataLength = 16000; // Example data length
      const header = generateWavHeader(pcmFormat, dataLength);

      validateWavHeader(header, dataLength);
    });
  });

  describe("encodePcmData", () => {
    test("should encode PCM data with the correct WAV header", () => {
      const pcmData = Buffer.alloc(16000); // Example PCM data
      const wavBuffer = encodePcmData(pcmFormat, pcmData);

      const header = wavBuffer.subarray(0, 44);
      const data = wavBuffer.subarray(44);

      validateWavHeader(header, pcmData.length);
      expect(data.equals(pcmData)).toBe(true);
    });
  });
});
